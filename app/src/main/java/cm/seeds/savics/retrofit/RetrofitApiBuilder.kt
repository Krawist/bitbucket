package cm.seeds.squarecm.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitApiBuilder {

    companion object{

        fun <T: Any> getService(baseUrl : String, clazz: Class<T>) : T{
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(clazz)
        }
    }

}