package cm.seeds.savics.retrofit

import cm.seeds.savics.models.Meal
import retrofit2.http.GET

interface ApiService {

    companion object{
        const val BASE_URL = "http://192.168.100.12:8001/"
    }


    @GET("meals")
    suspend fun loadMeals() : List<Meal>

}