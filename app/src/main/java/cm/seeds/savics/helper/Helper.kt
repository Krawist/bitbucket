package cm.seeds.savics.helper

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.DimenRes
import androidx.navigation.NavOptions
import cm.seeds.savics.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions


/**
 * App navigation options
 */
val navOptions = NavOptions.Builder()
    .setEnterAnim(R.anim.slide_in_right)
    .setExitAnim(R.anim.slide_out_left)
    .setPopExitAnim(R.anim.slide_out_right)
    .setPopEnterAnim(R.anim.slide_in_left)
    .build()


/**
 * Charge une image dans un imageView en utilisant la référence de celui ci
 * @param imageView
 * @param imageReference
 * @param isCircle
 */
fun loadImageInView(
    imageView: ImageView,
    imageReference: Any?,
    defaultResourceId: Int = R.drawable.frame_1,
    isCircle: Boolean = false
) {

    var glideManger = when (imageReference) {

        is String, is Uri -> Glide.with(imageView).load(imageReference).error(defaultResourceId)

        is Int -> Glide.with(imageView).load(imageReference).error(defaultResourceId)

        else -> null
    }

    if (glideManger != null) {

        if (isCircle) {
            glideManger = glideManger.circleCrop()
        }

        glideManger = glideManger.transition(DrawableTransitionOptions.withCrossFade())

        glideManger.into(imageView)

    }
}


/**
 * Retourne le nombre d'item de la taille fourni en paramètre
 */
fun numberOfItemInLine(activity: Activity, @DimenRes dimenRes: Int): Int {
    val display = activity.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    val with = size.x
    val oneItemWidth = activity.resources.getDimension(dimenRes)
    val numberOfItemInLine = with / oneItemWidth

    return numberOfItemInLine.toInt()
}


/**
 * Affiche un Toast
 */
fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}