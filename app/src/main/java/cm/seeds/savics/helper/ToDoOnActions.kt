package cm.seeds.squares_accounting.helper

import android.view.View

interface ToDoOnActions {
    fun onClick(any: Any, position: Int, viewClicked: View)
    fun onLongCLick(any: Any, position: Int, viewClicked: View) : Boolean{
        return false
    }
}