package cm.seeds.savics.ui.main

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import cm.seeds.savics.R
import cm.seeds.savics.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    private lateinit var databinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        databinding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(databinding.root)
    }
}