package cm.seeds.savics.ui.main.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cm.seeds.savics.R
import cm.seeds.savics.data.database.AppDatabase
import cm.seeds.savics.data.database.Repository
import cm.seeds.savics.models.MealCategory
import kotlinx.coroutines.launch
import kotlin.random.Random

class HomeViewModel(application: Application) : ViewModel() {

    private val dao = AppDatabase.database(application).mealDao()
    private val repository = Repository()

    val categories = MutableLiveData(listOf(
        MealCategory(image = R.drawable.grid, name = "All Products"),
        MealCategory(image = R.drawable.fruits, name = "Fruits"),
        MealCategory(image = R.drawable.vegetable, name = "Vegetables"),
        MealCategory(image = R.drawable.meat, name = "Meat and eggs"),
        MealCategory(image = R.drawable.fish, name = "Fish and Seafood")
    ))

    val meals = dao.allMealsLiveData()

    init {
        viewModelScope.launch {
            val allCategories = categories.value?: listOf()
            val mealsInDatabase = dao.laodAllMeals()
            if(mealsInDatabase.isNullOrEmpty()){
                val newMeals = repository.loadMeals()
                dao.saveMeals(newMeals.map { it.apply {
                    categorie = allCategories[Random.nextInt(allCategories.size)].name
                }})
            }
        }
    }
}