package cm.seeds.savics.ui.main.splash_screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import cm.seeds.savics.R
import cm.seeds.savics.databinding.FragmentOverviewBinding
import cm.seeds.savics.helper.navOptions
import cm.seeds.savics.models.Meal
import cm.seeds.savics.ui.main.details_meal.DetailsMealFragment

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class OverviewFragment : Fragment() {

    private lateinit var databinding : FragmentOverviewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        databinding = FragmentOverviewBinding.inflate(inflater,container,false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewPager()

        addActionsOnViews()

    }

    private fun addActionsOnViews() {
        databinding.startButton.setOnClickListener {
            findNavController().navigate(R.id.loginFragment, null, navOptions)
        }
    }

    private fun setupViewPager() {
        val adapter = AdapterViewPager(this)
        databinding.viewpager.adapter = adapter
        databinding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                setDotVisibility(listOf(position==0, position==1, position==2))
            }
        })
    }

    fun setDotVisibility(dotsVisible : List<Boolean>){
        for (i in 0..2){
            databinding.layoutDot[i].findViewById<View>(R.id.inside_dot).isVisible = dotsVisible[i]
        }
    }






    class AdapterViewPager(fragment: Fragment) : FragmentStateAdapter(fragment){
        override fun getItemCount(): Int {
            return 3
        }

        override fun createFragment(position: Int): Fragment {
            return HintFragment.newInstance(position)
        }
    }

    class HintFragment : Fragment(){

        private var index = 0

        companion object {

            const val INDEX = "POSITION"

            fun newInstance(index : Int) = HintFragment().apply {
                arguments = Bundle().apply {
                    putInt(INDEX,index)
                }
            }

        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            arguments?.let {
                index = it.getInt(DetailsMealFragment.HintFragment.INDEX)
            }

        }

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View {
            return ImageView(requireContext()).apply {
                scaleType = ImageView.ScaleType.CENTER_CROP
                setImageResource(
                    when (index) {
                        0 -> R.drawable.frame_1
                        1 -> R.drawable.frame_2
                        2 -> R.drawable.frame_3
                        else -> R.drawable.frame_1
                    }
                )
            }
        }

    }
}