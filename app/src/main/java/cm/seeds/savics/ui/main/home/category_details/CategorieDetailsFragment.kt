package cm.seeds.savics.ui.main.home.category_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cm.seeds.savics.R
import cm.seeds.savics.adapter.MealAdapter
import cm.seeds.savics.helper.navOptions
import cm.seeds.savics.helper.numberOfItemInLine
import cm.seeds.savics.helper.showToast
import cm.seeds.savics.models.Meal
import cm.seeds.savics.models.MealCategory
import cm.seeds.savics.ui.main.details_meal.DetailsMealFragment
import cm.seeds.savics.ui.main.home.HomeViewModel
import cm.seeds.savics.ui.main.inspiration.InspirationFragment
import cm.seeds.savics.viewmodel.ViewModelFactory
import cm.seeds.squares_accounting.helper.ToDoOnActions

class CategorieDetailsFragment : Fragment() {

    private lateinit var listview : RecyclerView
    private lateinit var homeViewModel : HomeViewModel
    private val mealAdapter = MealAdapter(layoutRes = R.layout.item_meal, toDoOnActions = object : ToDoOnActions{
        override fun onClick(any: Any, position: Int, viewClicked: View) {
            findNavController().navigate(R.id.detailsMealFragment,Bundle().apply {
                putSerializable(DetailsMealFragment.MEAL_TO_SHOW,any as Meal)
            }, navOptions)
        }
    })

    companion object{

        const val CATEGORY = "CATEGORY"

        fun newInstance(category: MealCategory) = CategorieDetailsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(CATEGORY,category)
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val category = arguments?.getSerializable(CATEGORY)

        if(category!=null){
            //We should initialise the viewmodel this fragment here, passing the specific category to show
            homeViewModel = ViewModelProvider(requireActivity(),ViewModelFactory(requireActivity().application,category = category as MealCategory))[HomeViewModel::class.java]
        }else{
            showToast(requireContext(),"No Category to show")
            requireActivity().onBackPressed()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        listview = RecyclerView(requireContext()).apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = mealAdapter
        }

        return listview
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        attachObservers()

    }

    private fun attachObservers() {
        homeViewModel.meals.observe(viewLifecycleOwner,{
            mealAdapter.submitList(it)
        })

    }

}