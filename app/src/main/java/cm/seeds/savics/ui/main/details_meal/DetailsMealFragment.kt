package cm.seeds.savics.ui.main.details_meal

import android.content.res.ColorStateList
import android.os.Build
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import cm.seeds.savics.R
import cm.seeds.savics.databinding.DetailsMealFragmentBinding
import cm.seeds.savics.helper.loadImageInView
import cm.seeds.savics.models.Meal

class DetailsMealFragment : Fragment() {

    companion object{
        const val MEAL_TO_SHOW = "MEAL_TO_SHOW"
    }

    private lateinit var databinding : DetailsMealFragmentBinding
    private lateinit var detailsMealViewModel: DetailsMealViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        detailsMealViewModel = ViewModelProvider(this)[DetailsMealViewModel::class.java]

        arguments?.let {
            detailsMealViewModel.setMeal(it.getSerializable(MEAL_TO_SHOW) as Meal)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        databinding = DetailsMealFragmentBinding.inflate(inflater,container,false)
        return databinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        attachObservers()

        addActionsOnViews()

    }

    private fun addActionsOnViews() {
        databinding.backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun attachObservers() {

        detailsMealViewModel.meal.observe(viewLifecycleOwner,{
            if(it!=null){
                addDataToViews(it)
            }
        })

    }

    private fun addDataToViews(it: Meal) {
        databinding.meal = it
        loadImageInView(databinding.imageMeal,it.image)

        databinding.viewpager.adapter = AdapterViewPager(this,it)
        databinding.tabLayout.setupWithViewPager(databinding.viewpager)
    }





    class AdapterViewPager(private val fragment: Fragment,private val meal : Meal) : FragmentStatePagerAdapter(fragment.childFragmentManager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
        override fun getCount(): Int {
            return 2
        }

        override fun getItem(position: Int): Fragment {
            return HintFragment.newInstance(meal,position)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when(position){

                0 -> fragment.getString(R.string.product_details)

                1 -> fragment.getString(R.string.nutrients_facts)

                else -> ""

            }
        }
    }

    class HintFragment : Fragment(){

        private var index = 0
        private var meal : Meal? = null

        companion object {

            const val INDEX = "POSITION"
            const val MEAL = "MEAL"

            fun newInstance(meal: Meal, index : Int) = HintFragment().apply {
                arguments = Bundle().apply {
                    putInt(INDEX,index)
                    putSerializable(MEAL,meal)
                }
            }

        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            arguments?.let {
                index = it.getInt(INDEX)
                meal = it.getSerializable(MEAL) as Meal
            }

        }



        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View {
            return TextView(requireContext()).apply {
                setBackgroundResource(R.drawable.home_search_background)
                backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(),R.color.green_light))
                gravity = Gravity.CENTER
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    setTextAppearance(android.R.style.TextAppearance_Material_Body1)
                }else{
                    setTextAppearance(requireContext(),android.R.style.TextAppearance_Material_Body1)
                }


                when(index){

                    0 -> {
                        //We chould show the product details
                        text = meal?.description
                    }

                    1 -> {
                        // We chould show the Nutrient facts
                        text = meal?.description
                    }

                }
            }
        }

    }

}