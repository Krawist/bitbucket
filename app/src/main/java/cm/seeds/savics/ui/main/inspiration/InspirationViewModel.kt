package cm.seeds.savics.ui.main.inspiration

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cm.seeds.savics.models.Meal

class InspirationViewModel : ViewModel() {

    val deserts = MutableLiveData(listOf("Dishes","Snaks","Desserts","Breakfast"))
    val selectedDesserts = MutableLiveData(listOf("Low calories","Easy differents"))

}