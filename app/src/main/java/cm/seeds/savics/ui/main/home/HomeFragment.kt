package cm.seeds.savics.ui.main.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import cm.seeds.savics.R
import cm.seeds.savics.adapter.MealAdapter
import cm.seeds.savics.databinding.HomeFragmentBinding
import cm.seeds.savics.helper.loadImageInView
import cm.seeds.savics.helper.navOptions
import cm.seeds.savics.models.MealCategory
import cm.seeds.savics.ui.main.home.category_details.CategorieDetailsFragment
import cm.seeds.savics.viewmodel.ViewModelFactory
import com.google.android.material.tabs.TabLayoutMediator

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var databinding : HomeFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        databinding = HomeFragmentBinding.inflate(inflater,container,false)
        return databinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(),ViewModelFactory(requireActivity().application))[HomeViewModel::class.java]

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        attachObservers()

        setupViewpager()

        addActionsOnViews()

    }

    private fun addActionsOnViews() {
        databinding.cardButton.setOnClickListener {
            findNavController().navigate(R.id.inspirationFragment,null, navOptions)
        }

        databinding.backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun setupViewpager() {
        databinding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
            }
        })
    }

    private fun attachObservers() {
        viewModel.categories.observe(viewLifecycleOwner,{
            updateCategories(it?: listOf())
        })
    }

    private fun updateCategories(it: List<MealCategory>) {
        databinding.viewpager.adapter = ViewpgareAdapter(it)
        TabLayoutMediator(databinding.tablayout,databinding.viewpager){ tab, position ->
            val tabView = LayoutInflater.from(requireContext()).inflate(R.layout.categorie_tab_item,null,false)
            val category = it[position]
            tabView.findViewById<TextView>(R.id.tab_label).text = category.name
            val imageview = tabView.findViewById<ImageView>(R.id.tab_image)
            loadImageInView(imageview,category.image)
            tab.customView = tabView
        }.attach()
    }


    inner class ViewpgareAdapter(private val mealCategories : List<MealCategory>) : FragmentStateAdapter(this){

        override fun getItemCount(): Int {
            return mealCategories.size
        }

        override fun createFragment(position: Int): Fragment {
            return CategorieDetailsFragment.newInstance(mealCategories[position])
        }
    }

}