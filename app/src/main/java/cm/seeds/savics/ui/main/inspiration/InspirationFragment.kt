package cm.seeds.savics.ui.main.inspiration

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import cm.seeds.savics.R
import cm.seeds.savics.adapter.MealAdapter
import cm.seeds.savics.databinding.InspirationFragmentBinding
import cm.seeds.savics.helper.navOptions
import cm.seeds.savics.models.Meal
import cm.seeds.savics.ui.main.details_meal.DetailsMealFragment
import cm.seeds.savics.ui.main.home.HomeViewModel
import cm.seeds.savics.viewmodel.ViewModelFactory
import cm.seeds.squares_accounting.helper.ToDoOnActions
import com.google.android.material.chip.Chip

class InspirationFragment : Fragment() {

    private lateinit var viewModel: InspirationViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var databinding : InspirationFragmentBinding
    private val adapterMeal = MealAdapter(layoutRes = R.layout.item_meal_big_card, toDoOnActions = object  : ToDoOnActions{
        override fun onClick(any: Any, position: Int, viewClicked: View) {
            findNavController().navigate(R.id.detailsMealFragment,Bundle().apply {
                putSerializable(DetailsMealFragment.MEAL_TO_SHOW, any as Meal)
            }, navOptions)
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(InspirationViewModel::class.java)
        homeViewModel = ViewModelProvider(requireActivity(), ViewModelFactory(requireActivity().application)).get(HomeViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        databinding = InspirationFragmentBinding.inflate(inflater,container,false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupList()

        addActionsOnViews()

        attachObservers()

    }

    private fun addActionsOnViews() {
        databinding.backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun setupList() {

        databinding.listMeal.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterMeal
        }

    }

    private fun attachObservers() {

        viewModel.deserts.observe(viewLifecycleOwner,{
            if(!it.isNullOrEmpty()){
                databinding.layoutDeserts.removeAllViews()
                it.forEach { dessert ->
                    val chipView = LayoutInflater.from(requireContext()).inflate(R.layout.item_dessert,databinding.layoutDeserts,false)
                    chipView.findViewById<Chip>(R.id.chip).text = dessert
                    databinding.layoutDeserts.addView(chipView)
                }
            }
        })

        homeViewModel.meals.observe(viewLifecycleOwner,{
            adapterMeal.submitList(it)
        })

        viewModel.selectedDesserts.observe(viewLifecycleOwner,{
            if(!it.isNullOrEmpty()){
                databinding.selectedChipGroup.removeAllViews()
                it.forEach { dessert ->
                    val chipView = LayoutInflater.from(requireContext()).inflate(R.layout.item_dessert_selected,databinding.selectedChipGroup,false)
                    chipView.findViewById<Chip>(R.id.chip).text = dessert
                    databinding.selectedChipGroup.addView(chipView)
                }
            }
        })

    }


}