package cm.seeds.savics.ui.main.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import cm.seeds.savics.R
import cm.seeds.savics.databinding.FragmentLoginBinding
import cm.seeds.savics.helper.navOptions


class LoginFragment : Fragment() {

    private lateinit var databinding : FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        databinding = FragmentLoginBinding.inflate(inflater,container,false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addActionsToViews()

    }

    private fun addActionsToViews() {
        databinding.backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }

        databinding.buttonRegister.setOnClickListener {
            findNavController().navigate(R.id.homeFragment, null, navOptions)
        }
    }

}