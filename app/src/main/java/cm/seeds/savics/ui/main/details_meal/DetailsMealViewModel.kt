package cm.seeds.savics.ui.main.details_meal

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cm.seeds.savics.models.Meal

class DetailsMealViewModel : ViewModel() {

    val meal : MutableLiveData<Meal> = MutableLiveData()

    fun setMeal(meal: Meal){
        this.meal.value = meal
    }

}