package cm.seeds.savics.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cm.seeds.savics.models.MealCategory
import cm.seeds.savics.ui.main.home.HomeViewModel

class ViewModelFactory(private val application: Application, private val category: MealCategory? = null) : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return when{

            modelClass.isAssignableFrom(HomeViewModel::class.java) -> HomeViewModel(application = application) as T

            else -> throw IllegalArgumentException("Ce Viewmodel n'est pas founir par ce Factory")
        }
    }
}