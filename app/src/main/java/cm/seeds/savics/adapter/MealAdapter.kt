package cm.seeds.savics.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cm.seeds.savics.R
import cm.seeds.savics.databinding.ItemMealBigCardBinding
import cm.seeds.savics.databinding.ItemMealBinding
import cm.seeds.savics.helper.loadImageInView
import cm.seeds.savics.models.Meal
import cm.seeds.squares_accounting.helper.ToDoOnActions

class MealAdapter(@LayoutRes private val layoutRes : Int = R.layout.item_meal, private val toDoOnActions: ToDoOnActions) : ListAdapter<Meal, MealAdapter.MealViewHolder>(object  : DiffUtil.ItemCallback<Meal>(){
    override fun areItemsTheSame(oldItem: Meal, newItem: Meal): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Meal, newItem: Meal): Boolean {
        return oldItem == newItem
    }
}){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        return MealViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context),layoutRes,parent,false))
    }

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) {
        holder.bindData(getItem(position),position)
    }

    inner class MealViewHolder(private val databinding : ViewDataBinding) : RecyclerView.ViewHolder(databinding.root){

        private val imageMeal = databinding.root.findViewById<ImageView>(R.id.image_meal)

        fun bindData(meal: Meal, position : Int){
            when(databinding){

                is ItemMealBinding -> databinding.meal = meal

                is ItemMealBigCardBinding -> databinding.meal = meal

            }

            loadImageInView(imageMeal,meal.image)

            databinding.root.setOnClickListener {
                toDoOnActions.onClick(meal,position,it)
            }
        }

    }
}