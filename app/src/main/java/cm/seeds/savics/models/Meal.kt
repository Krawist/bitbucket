package cm.seeds.savics.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Meal(

    @PrimaryKey
    val id : Long = System.currentTimeMillis(),
    val name : String = "",
    val description : String = "",
    val image : String = "",
    val price : Int = 0,
    var categorie : String = ""
) : Serializable{

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Meal

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (image != other.image) return false
        if (price != other.price) return false
        if (categorie != other.categorie) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + image.hashCode()
        result = 31 * result + price
        result = 31 * result + categorie.hashCode()
        return result
    }
}