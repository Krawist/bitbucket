package cm.seeds.savics.models

import java.io.Serializable

data class MealCategory(
    val image : Int,
    val name : String
) : Serializable