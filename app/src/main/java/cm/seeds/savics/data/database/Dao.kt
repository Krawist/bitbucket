package cm.seeds.savics.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Dao
import cm.seeds.savics.models.Meal

@Dao
interface Dao {


    /**
     * Method that save meals to database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveMeals(meals : List<Meal>)


    /**
     * Method that delete meals
     */
    @Delete
    suspend fun deleteMeals(meals: List<Meal>)

    /**
     * Method that laod all saved meal in a livedaa
     */
    @Query("SELECT * FROM meal")
    fun allMealsLiveData() : LiveData<List<Meal>>


    /**
     * Method that load all saved meal in a list
     */
    @Query("SELECT * FROM meal")
    suspend fun laodAllMeals() : List<Meal>

}