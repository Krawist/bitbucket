package cm.seeds.savics.data.database

import cm.seeds.savics.retrofit.ApiService
import cm.seeds.squarecm.retrofit.RetrofitApiBuilder

class Repository {

    private val apiService = RetrofitApiBuilder.getService(ApiService.BASE_URL,ApiService::class.java)

    suspend fun loadMeals() = apiService.loadMeals()

}