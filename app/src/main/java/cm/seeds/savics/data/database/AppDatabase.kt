package cm.seeds.savics.data.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cm.seeds.savics.models.Meal

@Database(
    version = 1,
    entities = [Meal::class]
)
abstract class AppDatabase : RoomDatabase() {

    companion object{

        private var instance : AppDatabase? = null

        fun database(application: Application) = instance?: synchronized(this){
            instance?:buildDatabase(application).also { instance = it }
        }

        private fun buildDatabase(application: Application) = Room.databaseBuilder(application, AppDatabase::class.java, "meals.db")
            .fallbackToDestructiveMigration()
            .build()

    }

    abstract fun mealDao() : Dao

}